#To Do List for: *bzflag*
---------------------
##encapsulate stuff requiring platform *#ifdef's*:
   * Networking API into libNet.a.
   fork/exec (used in menus.cxx) into libPlatform.a
   file system stuff (dir delimiter, etc.)
   user name stuff

* Write *bzfbiff*

* Watch server for players  

* Run program when somebody joins

* Windows *makefiles*

##Currently have [*Microsoft Visual C++*](https://www.microsoft.com/en-us/download/details.aspx?id=53840) projects only:

1. Auto-generated dependency support in makefiles
   manual dependencies cause too much trouble

2. Smarter robots

3. Add type of shot *(normal, gm, sw, etc)* to killed message

4. Support resolutions besides **640x480** on passthrough 3Dfx cards

git add .git
git status