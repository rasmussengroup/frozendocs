# Drone Pilot Game

----
## See Instructions below:
![Drone Image](https://www.geekstips.com/wp-content/uploads/2017/01/military-drone-geekstips.jpg)

> This will explain the way the drone game works.

----
### Steps
1. Start Game.
2. Drone is activated.
3. Drone is located.
4. Fire at drone.
5. Was the fire a hit?
  * If no, repeat step 4.
  * If yes, proceed to step 6.
6. Was drone destroyed?
 * If no, repeat step 4.
 * If yes, proceed to step 7.
7. Drone Destroyed.
8. End Game.

----

#### Thanks for playing!
