## Markdown Tutorial
- Italics and Bold

To emphasize text with italics, add an asterisk (*). Do this for the word 'awesome'
in the next sentence.

This is an *awesome* course!

To make text bold, surround it with two asterisks (**). Do this for the word
'really'.

This is a **really** awesome course!

You can mix italics and bold in the same sentence. Change 'Naturally' to italics and make 'work hard' bold.

*"Naturally"*, she said, "I realize that I'm going to have to **work hard** to succeed."

You can make text both bold and italic by surrounding it with
three asterisks (***). Do that with the phrase 'Markdown seems really easy'

You're probably thinking to yourself,* **Markdown seems really easy.*** 

**(I had to place a space between the *'s as it would not work!)**

- Headers

Headers add emphasis to a section, like a title. To make a header, simply preface the text with a hash symbol (#), followed by a space. There are six levels of headers and you use the number of hashes for the size you want -- One hash is the largest, six hashes is the smallest. Turn the text below into the appropriate size header.

#This is a one-hash header.

##This is a two-hash header.

###This is a three-hash header.

####This is a four-hash header.

#####This is a five-hash header.

######This is a six-hash header.

Bold doesn't work with headers, but italics do. Make the following line a level two header and italicize the movie title.

##A Review of *Batman vs. Superman*

- Links

You can also link to Web sites using Markdown. There are two link types. The first is called an inline link. To make one, put the link text (the part that you click on) in square brackets ([ ]) and put the link (the Web address or URL) in parentheses. In the next sentence, make 'Rasmussen College' the link text and link it to www.rasmussen.edu.

NOTE: The link has to immediately follow the link text.

Find out more about [Rasmussen College](http://www.rasmussen.edu/)

You can emphasize link text. Make the phrase 'a lot' bold and make the entire sentence link to www.google.com

[You're going to be going to this Web site **a lot**.](https://www.google.com/)

You can also make links in headers. Make the next sentence a level 3 header and link 'Hollywood News' to www.imdb.com

###The Latest [Hollywood News](http://www.imdb.com/)

Reference links are a little more complicated but they have an advantage over inline links. If you have multiple links in your document to the same place, you only have to update the link once. Reference links are beyond the scope of this tutorial but we'll come back to them later in the course.

Now that you know how to make links, you can embed images in your Markdown documents. An image is preceded by an exclamation point (!) followed by our square brackets and then parentheses with the link to the image. The difference is that the square brackets contain the 'alt text', which is the description that you get when you hover over the image. This is designed for visually impaired users.

Create an image link with the alt text 'A typical city-scape' and the link to http://lorempixel.com/320/240/city

![](http://lorempixel.com/320/240/city)

- Blockquotes

Blockquotes are useful when you want to call attention to a quote from another source. All you have to do is preface the quote with a greater-than (>) sign. Turn this quote into a blockquote.

>"All programmers are playwrights and all computers are lousy actors."

If your quote spans more than one paragraph, put the greater-than sign on each paragraph, including the blank lines. Make the next two quotes into a single blockquote.

>"Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program."
>
>"I'm perfectly happy complaining, because it's cathartic, and I'm perfectly happy arguing with people on the Internet because arguing is my favorite pastime - not programming."

You can also add italics, images and links to blockquotes.

- Lists

There are two types of lists -- unordered and ordered. Unordered is just another name for bullet lists. An ordered list has numbers or letters.

To make an unordered list, just add asterisks to each list element, followed by a space. In the next line, turn each element separated by commas into a list.

* Pick up laundry 
* Gas up car 
* Get cat food

Ordered lists look just like you think they'd look. You add a number, a period and a space before each element in the list (1. ).

Turn the recipe below into an ordered list of steps.

1. Pre-heat the oven to 350 
2. Toss the carrots in olive oil and crushed garlic
3. Put carrots in baking dish
4. roast for 1 hour and stir every fifteen minutes

You can also add italics, bold or links to the elements of a list.

You can make sublists by just adding a tab before the list element.

You can also mix ordered and unordered lists.

- Paragraphs

If you want to separate your text into paragraphs you can use either a soft break or a hard break. A soft break is done by adding two spaces right after the end of the line before the next paragraph. Separate the next two lines into two paragraphs with a soft break.

This is the first paragraph.  
This is the second paragraph.

A hard break is adding an extra carriage return at the end of a line. This also has the effect of adding an empty line between paragraphs. Turn the two lines below into separate paragraphs with a hard return.

This paragraph will be followed by a blank line by using the hard return.

This paragraphs comes after the hard return, so there's a blank line before it.

*Congratulations!* You have the basics of [Markdown](https://daringfireball.net/projects/markdown/basics)!


