### Modified README on BitBucket - 02/20/2018
#Project: FrozenFist
##Company: *VortexOps*
######Designing software for semi-autonomous armored assault vehicles. The vehicles are designed to operate either remotely with a human pilot or in *"robot mode"* with guidance from an on-board Artificial Intelligence (AI) system.

email: [Daniel.Sanabria@smail.rasmussen.edu](https://outlook.office.com/owa/?realm=rasmussen.edu)

![](http://greenscreenlab.com/wp-content/uploads/2017/03/Free-Stock-Photography-Website-Pixabay1.jpg)